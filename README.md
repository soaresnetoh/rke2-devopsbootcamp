# [RKE2](https://docs.rke2.io/) - [DevopsBootcampLive](https://www.youtube.com/watch?v=xEH8lBpsCtQ)

Este repositório foi criado para os estudos sobre a talk ministrada pelo [Kaio Neuhauss](https://www.linkedin.com/in/kaioneuhauss/) sobre [RKE2](https://docs.rke2.io/) no canal do [DevopsBootCamp](https://www.devopsbootcamp.net/), junto com a [Amanda](https://www.linkedin.com/in/amandasilvapinto/) que abrange praticamente todo conteúdo.

O Link da live é: https://www.youtube.com/watch?v=xEH8lBpsCtQ

## O que é:

o [RKE2](https://docs.rke2.io/) é uma ferramenta de criação de cluster kubernetes focado em seguraça e em conformidade com a [FIPS 140-2](https://docs.rke2.io/security/fips_support)

## Como Usar:

- Neste projeto estamos usando como nosso aliado o [vagrant](https://www.vagrantup.com/) para subir a infra.

Voce precisa seguir os [requisitos de hardware são de 4GB-Ram e 2vCPU ](https://docs.rke2.io/install/requirements#linuxwindows) para ter uma boa performance, porem eu estou iniciando os testes com metade disso.


```bash
vagrant up # sobre a vm
vagrant ssh # acessa por ssh a vm
sudo su # sobre os privilegios para root
kubectl get nodes 
```

Para excluir tudo, use:

```bash
vagrant destroy
```